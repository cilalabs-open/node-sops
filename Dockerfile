FROM node:16-alpine3.14
RUN apk add --no-cache --update curl
RUN curl -L https://github.com/mozilla/sops/releases/download/v3.7.1/sops-v3.7.1.linux -o /usr/local/bin/sops && \
    chmod +x /usr/local/bin/sops
RUN apk del curl
ENTRYPOINT ["/bin/sh", "-c"]
